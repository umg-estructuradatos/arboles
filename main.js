const { BinarySearchTree } = require("./common/binary-search-tree");
const BST = new BinarySearchTree();

//Insert some nodes
//          15
//         /  \
//       10    25
//      /  \   / \
//     7   13 22  27s
//    / \    / 
//   5   9  17

BST.insert(15);
BST.insert(25);
BST.insert(10);
BST.insert(7);
BST.insert(22);
BST.insert(17);
BST.insert(13);
BST.insert(5);
BST.insert(9);
BST.insert(27);

/*
Ejercicio:
¿Si tienes un árbol BST con el nodo raíz con valor (4) y con un hijo a la izquierda con valor (2), y a la derecha otro hijo con valor (5), al insertar un nodo con valor (3), donde deberia ser colocado?
hijo izq (2)
hijo izq de (5)
hijo derecha de (2)
insertar bajo la raíz (4)
*/
//Insert some nodes
//          4
//         /  \
//        2    5
//         \   
//           3
/*
BST.insert(4);
BST.insert(3);
BST.insert(2);
BST.insert(5);
*/

// inOrden
const root = BST.getRootNode();
let arrayResults = [];
BST.inOrder(root, (node)=> arrayResults.push(node.data));
console.log("inOrder \n \t" + arrayResults);

arrayResults = [];
BST.preOrder(root, (node)=> arrayResults.push(node.data));
console.log("preOrder \n \t" + arrayResults);

arrayResults = [];
BST.postOrder(root, (node)=> arrayResults.push(node.data));
console.log("postOrder \n \t" + arrayResults);

//Buscar nodo 
console.log(BST.searchNode(root, 25));